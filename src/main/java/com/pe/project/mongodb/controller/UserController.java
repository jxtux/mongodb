package com.pe.project.mongodb.controller;

import com.pe.project.mongodb.domain.User;
import com.pe.project.mongodb.repository.UserRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.domain.Sort.Order;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class UserController {

//    @Autowired
//    UserRepository userRepository;
    private UserRepository userRepository;

    @Autowired
    public void setProductToProductForm(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @GetMapping("/userList/{user}")
    public List<User> findListUser(@PathVariable String user) {
        List<User> l = userRepository.findCustomByRegExUser(user);

        return l;
    }

    @GetMapping("/userListAll/{pagina}")
    public List<User> findListUserAll(@PathVariable Integer pagina) {
        Page<User> l = userRepository.findAll(new PageRequest(pagina,20,new Sort( new Order(Direction.ASC, "name"), 
                                                                                  new Order(Direction.DESC, "user"))));
        
        return l.getContent();
    }

    @PostMapping(path = "/public/message")
    public String postAddMessage() {
        return "Post Mapping";
    }

}
