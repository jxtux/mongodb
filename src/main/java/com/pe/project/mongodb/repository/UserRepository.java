package com.pe.project.mongodb.repository;

import com.pe.project.mongodb.domain.User;
import java.util.List;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

public interface UserRepository extends MongoRepository<User, Long> {

    User findByUserAndName(String user, String name);

    //Supports native JSON query string
    @Query("{user:'?0'}")
    User findCustomByUser(String user);

    @Query(value="{user: { $regex: ?0 } }", fields="{ 'user' : 1, 'name' : 1}")
    List<User> findCustomByRegExUser(String user);
}
