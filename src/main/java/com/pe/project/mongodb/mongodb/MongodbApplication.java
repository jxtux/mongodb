package com.pe.project.mongodb.mongodb;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

@SpringBootApplication
@EntityScan(basePackages="com.pe.project.mongodb.domain")
@EnableMongoRepositories(basePackages = "com.pe.project.mongodb.repository")
@ComponentScan("com.pe.project.mongodb.controller")

public class MongodbApplication {

    public static void main(String[] args) {
        SpringApplication.run(MongodbApplication.class, args);
    }
}
