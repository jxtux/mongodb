package com.pe.project.mongodb.mongodb;

import com.pe.project.mongodb.domain.User;
import com.pe.project.mongodb.repository.UserRepository;
import java.util.List;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class MongodbApplicationTests {

    @Autowired
    UserRepository userRepository;

    @Autowired
    private MongoOperations mongoOps;

//    @Test
    public void insertUser() {
        User u = new User();
        u.setIdentificador(3);
        u.setUser("leopardo5x");
        u.setName("Alexis City");
        u.setEdad(20);
        userRepository.insert(u);
    }

    @Test
    public void updatetUser() {
        User user = new User();
        user = mongoOps.findOne(Query.query(Criteria.where("user").is("jxtux")), User.class);

        user.setName("Antonio 5x");
        userRepository.save(user);
    }

//    @Test
    public void contextLoads() {
        List<User> l = userRepository.findCustomByRegExUser("j");

        for (User user : l) {
            System.out.println(user.getName());
        }
    }

}
